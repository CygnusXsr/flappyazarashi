﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AzarashiController : MonoBehaviour {
    Rigidbody2D rb2d;
    Animator animator;
    float angle;
    bool isDead;
    public float maxHeight;
    public float flapVelocity;
    public float relativeVelocityX;
    //Spriteオブジェクトの参照
    public GameObject sprite;
    public bool IsDead()
    {
        return isDead;
    }
    //Awake関数によるコンポーネントの取得
    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        //Animatorコンポーネントの取得
        animator = sprite.GetComponent<Animator>();
    }
    void Update () {
		//最高高度に達していない場合に限りタップの入力を受け付ける 入力の判定
        if(Input.GetButtonDown("Fire1") && transform.position.y < maxHeight)
        {
            Flap();
        }
        //角度を反映
        ApplyAngle();
        //angleが水平以上だったら、アニメーターのflapフラグをtrueにする アニメーションステートの制御
        animator.SetBool("flap", angle >= 0.0f);
	}
    public void Flap()
    {
        //死んだら羽ばたけない isDeadフラグによるFlap制御
        if (isDead) return;
        //重力が効いていないときは操作しない
        if (rb2d.isKinematic) return;
        //Velocityを直接書き換えて上方向に加速 速度の操作
        rb2d.velocity = new Vector2(0.0f, flapVelocity);
    }
    void ApplyAngle()
    {
        //現在の速度、相対速度から進んでいる角度を求める
        float targetAngle;
        //死亡したら常に下を向く
        if (isDead)
        {
            targetAngle = -90.0f;
        }
        else
        {
            targetAngle = Mathf.Atan2(rb2d.velocity.y, relativeVelocityX) * Mathf.Rad2Deg;
        }
        //回転アニメをスムージング
        angle = Mathf.Lerp(angle, targetAngle, Time.deltaTime * 7.0f);
        //Rotationの反映
        sprite.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, angle);
    }
    //コリジョンによる死亡判定
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isDead) return;
        //何かにぶつかったら死亡フラグをたてる
        isDead = true;
    }
    public void SetSteerActive(bool active)
    {
        //Rigidbodyのオン、オフを切り替える
        rb2d.isKinematic = !active;
    }
}
