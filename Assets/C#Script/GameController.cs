﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
    //ゲームステートの定義
    enum State
    {
        Ready,Play,GameOver
    }
    State state;
    public AzarashiController azarashi;
    public GameObject blocks;
	void Start () {
        //開始と同時にReadyステートに移行
        Ready();
	}
	void LateUpdate () {
        //ゲームのステートごとにイベントを監視
        switch (state)
        {
            case State.Ready:
                //タッチしたらゲームスタート
                if (Input.GetButtonDown("Fire1")) GameStart();
                break;
        }
	}
    void Ready()
    {

    }
}
